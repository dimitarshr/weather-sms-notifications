from weatherInformation import forecast
import requests

class weatherAPI:
    def __init__(self, api_key):
        self.__api_key = api_key
        self.__locationName = ''


    def setGeoLocation(self, locationName):
        urlLocation = "http://maps.googleapis.com/maps/api/geocode/json?address={}".format(locationName)
        jsonText = requests.get(urlLocation).json()

        coordinates = jsonText['results'][0]['geometry']['location']
        cityName = jsonText['results'][0]['address_components'][1]
        regionName = jsonText['results'][0]['address_components'][2]
        self.__latCoord = coordinates['lat']
        self.__lngCoord = coordinates['lng']
        self.__locationName = str(cityName['short_name']) +", "+ str(regionName['short_name'])
        print self.__latCoord
        print self.__lngCoord
        print self.__locationName


    def getCurrentWeather(self):
        forecastText = forecast(self.__api_key, self.__latCoord, self.__lngCoord).getCurrentWeather()

        return forecastText

    def getNextHour(self):
        forecastText = forecast(self.__api_key, self.__latCoord, self.__lngCoord).getFutureWeather(0)

        return forecastText

    def getTomorrow(self):
        forecastText = forecast(self.__api_key, self.__latCoord, self.__lngCoord).getFutureWeather(1)

        return forecastText