import datetime
import requests

#REFACTOR AND ADD THE LOCATION NAME

class forecast:
    def __init__(self, api_key, lat, lng):
        self.__api_key = api_key
        self.__lat = lat
        self.__lng = lng
        self.__units = 'uk'

    def __getFullForecast(self):
        __url = 'https://api.darksky.net/forecast/{}/{},{}?units={}'\
            .format(self.__api_key, self.__lat, self.__lng, self.__units)
        __fullForecast = requests.get(__url)

        __forecastJSON = __fullForecast.json()

        return __forecastJSON

    def getCurrentWeather(self):
        self.__currentWeather = self.__getFullForecast()['currently']

        try:
            self.__time = datetime.datetime.utcfromtimestamp(int(self.__currentWeather['time'])) \
                        + datetime.timedelta(hours=1)
        except:
            self.__time = "Error recording time."

        try:
            self.__precipProbability = float(self.__currentWeather['precipProbability']) * 100
        except:
            self.__precipProbability = "Error recording Precipitaion probability."

        try:
            self.__temperature = int(self.__currentWeather['temperature'])
        except:
            self.__temperature = "Error recording temperature."

        __forecastBody = "Time: " + str(self.__time)+'\n' + \
                       "\tPrecipitation probability in %: " + str(self.__precipProbability)+'\n' + \
                       "\tTemperature: " + str(self.__temperature)+'\n'

        return __forecastBody

    def __getNextHour(self):
        self.__nextHour = self.__getFullForecast()['hourly']
        self.__summary = self.__nextHour['summary']
        self.__dataNextHour = self.__nextHour['data'][1]

        try:
            self.__time = datetime.datetime.utcfromtimestamp(int(self.__dataNextHour['time'])) \
                        + datetime.timedelta(hours=1)
        except:
            self.__time = "Error recording time."

        try:
            self.__precipProbability = float(self.__dataNextHour['precipProbability']) * 100
        except:
            self.__precipProbability = "Error recording Precipitaion probability."

        try:
            self.__temperature = int(self.__dataNextHour['temperature'])
        except:
            self.__temperature = "Error recording temperature."

        __forecastBody = "Time: " + str(self.__time)+'\n' + \
                       "\tPrecipitation probability in %: " + str(self.__precipProbability)+'\n' + \
                       "\tTemperature: " + str(self.__temperature)+'\n'

        return __forecastBody

    def __getTomorrow(self):
        self.__tomorrow = self.__getFullForecast()['daily']
        self.__summary = self.__tomorrow['summary']
        self.__dataTomorrow = self.__tomorrow['data'][1]

        try:
            self.__time = datetime.datetime.utcfromtimestamp(int(self.__dataTomorrow['time'])) \
                        + datetime.timedelta(hours=1)
        except:
            self.__time = "Error recording time."

        try:
            self.__precipProbability = float(self.__dataTomorrow['precipProbability']) * 100
        except:
            self.__precipProbability = "Error recording Precipitaion probability."

        try:
            self.__temperature = int(self.__dataTomorrow['apparentTemperatureMax'])
        except:
            self.__temperature = "Error recording temperature."

        __forecastBody = "\nWeather: " + \
                         "\nTime: " + str(self.__time)+'\n' + \
                       "\tPrecipitation probability in %: " + str(self.__precipProbability)+'\n' + \
                       "\tTemperature: " + str(self.__temperature)+'\n'

        return __forecastBody

    def getFutureWeather(self, option=0):
        if option == 0:
            return self.__getNextHour()
        elif option == 1:
            return self.__getTomorrow()