import requests

class Customer:
    def __init__(self,userName, accountSID, accountToken):
        self.__userName = userName
        self.__accountSID = accountSID
        self.__accountToken = accountToken
        self.__twilioVersion = '2010-04-01'

    def sendNotification(self, toNumber, fromNumber, bodyText):
        #Values used as variables in the request POST method
        __messageParamenters = {'To': str(toNumber), 'From': str(fromNumber),'Body': bodyText}
        __url = 'https://{}:{}@api.twilio.com/{}/Accounts/{}/Messages'\
            .format(self.__accountSID, self.__accountToken, self.__twilioVersion, self.__accountSID)

        requests.post(__url, data=__messageParamenters)

    def weatherRequest(self):
        __url = 'https://{}:{}@api.twilio.com/{}/Accounts/{}/Messages'\
            .format(self.__accountSID, self.__accountToken, self.__twilioVersion, self.__accountSID)

        messages = requests.get(__url)

        print messages